// 字典
export const UPLOADTYPE = {
    'video/mp4':'mp4',
    'video/mp3':'mp3'
}
export const UPLOADINFO = {
    'NO_INFO':'请你您选择文件进行上传',
    'TYPE_INFO':'您上传的文件格式不正确,仅支持mp4，mp3',
    'UPLOAD_LOADING':'上传中',
    'UPLOAD_ERROR':'上传失败了请您检查网络',
    'UPLOAD_SUCCESS':'上传成功'
}
// 路径
export const BASEURL = 'http://localhost:8000/file/upload'
// 每次切多少
export const CHUNKSIZE =  64 * 1024
